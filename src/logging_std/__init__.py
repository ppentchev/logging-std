# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Build a logger that outputs to the standard output and error streams."""

from __future__ import annotations

import functools
import logging
import sys
import typing


if typing.TYPE_CHECKING:
    from typing import Final, TextIO


VERSION: Final = "0.1.0"
"""The version of the `logging-std` library, SemVer-like."""


@functools.lru_cache
def build_logger(
    *,
    verbose: bool = False,
    error_stream: TextIO | None = None,
    info_stream: TextIO | None = None,
) -> logging.Logger:
    """Build a logger that outputs to the standard output and error streams.

    Messages of level `INFO` go to the standard output stream.
    Messages of level `WARNING` and higher go to the standard error stream.
    If `verbose` is true, messages of level `DEBUG` also go to the standard error stream.

    The stream to send error messages to may be overridden by the `error_stream` parameter,
    and the one for informational messages - by the `info_stream` one.
    """
    logger: Final = logging.getLogger("logging_std")
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.propagate = False

    if error_stream is None:
        error_stream = sys.stderr
    diag_handler: Final = logging.StreamHandler(error_stream)
    diag_handler.setLevel(logging.DEBUG if verbose else logging.WARNING)
    diag_handler.addFilter(lambda rec: rec.levelno != logging.INFO)
    logger.addHandler(diag_handler)

    if info_stream is None:
        info_stream = sys.stdout
    info_handler: Final = logging.StreamHandler(info_stream)
    info_handler.setLevel(logging.INFO)
    info_handler.addFilter(lambda rec: rec.levelno == logging.INFO)
    logger.addHandler(info_handler)

    return logger


@functools.lru_cache
def build_single_logger(
    *,
    verbose: bool = False,
    stream: TextIO | None = None,
) -> logging.Logger:
    """Build a logger that outputs to the standard error stream.

    Messages of level `INFO` and higher go to the standard error stream.
    If `verbose` is true, messages of level `DEBUG` also go to the standard error stream.

    The stream to send messages to may be overridden by the `stream` parameter.
    """
    logger: Final = logging.getLogger("logging_std")
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.propagate = False

    if stream is None:
        stream = sys.stderr
    diag_handler: Final = logging.StreamHandler(stream)
    diag_handler.setLevel(logging.DEBUG if verbose else logging.INFO)
    logger.addHandler(diag_handler)

    return logger
