<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# API Reference

## Build a simple logger

::: logging_std.build_logger
::: logging_std.build_single_logger
