<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# logging-std - build a simple logger

<!--
\[[Home][ringlet-logging-std] | [GitLab][gitlab] | [PyPI][pypi] | [ReadTheDocs][readthedocs]\]
-->
\[[Home][ringlet-logging-std] | [GitLab][gitlab]\]

## Overview

The `logging-std` library provides a `build_logger()` function to help programs that
want to log messages to the standard output and standard error streams.

## Build a simple logger

The [build_logger()][logging_std.build_logger] function will return
a singleton `logging.Logger` object (via `functools.lru_cache()`) that sends:

- error and warning messages to the standard error stream
- informational messages to the standard output stream
- debug messages to the standard error stream if the `verbose` option is
  set; otherwise they are ignored

The text stream to send error, warning, and diagnostic messages to may be
overridden by the `error_stream` argument.
The text stream to send informational messages to may be
overridden by the `info_stream` argument.

## Example

```python
    args: Final = parse_command_line_arguments()
    cfg: Final = Config(
        filenames=[pathlib.Path(fname).resolve() for fname in args.filenames],
        log=logging_std.build_logger(verbose=args.verbose),
    )
    cfg.log.info("Starting up")
    cfg.log.debug(
        "Files to process: %(files)s",
        {
            "files": " ".join(sorted(cfg.filenames))
        }
    )
```

## Build a simple logger with a single output stream

The [build_single_logger()][logging_std.build_single_logger] function will return
a singleton `logging.Logger` object (via `functools.lru_cache()`) that sends:

- error, warning, and informational messages to the standard error stream
- debug messages to the standard error stream if the `verbose` option is
  set; otherwise they are ignored

The text stream to send messages to may be overridden by the `stream` argument.

## Example

```python
    args: Final = parse_command_line_arguments()
    cfg: Final = Config(
        filenames=[pathlib.Path(fname).resolve() for fname in args.filenames],
        log=logging_std.build_single_logger(verbose=args.verbose),
    )
    cfg.log.info("Starting up")
    cfg.log.debug(
        "Files to process: %(files)s",
        {
            "files": " ".join(sorted(cfg.filenames))
        }
    )
```

## Contact

The `logging-std` library was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab].
This documentation is hosted at [Ringlet][ringlet-logging-std].
<!--
with a copy at [ReadTheDocs][readthedocs].
-->

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/logging-std "The logging-std GitLab repository"
[pypi]: https://pypi.org/project/logging-std/ "The logging-std Python Package Index page"
[readthedocs]: https://logging-std.readthedocs.io/ "The logging-std ReadTheDocs page"
[ringlet-logging-std]: https://devel.ringlet.net/devel/logging-std/ "The Ringlet logging-std homepage"
