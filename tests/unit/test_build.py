# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test the `build_logger()` function."""

from __future__ import annotations

import typing

import pytest

from . import util


if typing.TYPE_CHECKING:
    from typing import Final


class ReprStr:
    """An object that gives different results for `repr()` and `str()`."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return "<a ReprStr object>"

    def __repr__(self) -> str:
        """Provide a Python-eque representation."""
        return "ReprStr()"


MSG_ERR: Final = "This is an error message"
MSG_WARNING: Final = "This is a warning message"
MSG_INFO: Final = "This is an info message"
MSG_DEBUG: Final = "This is a debug message"
MSG_DEBUG_2: Final = "This is another debug message"

MSG_DEBUG_NAME: Final = "Hello, %(name)s!"
MSG_ERR_RS: Final = "Do we have %(obj)s?"
MSG_INFO_NAME_RS_R: Final = "The name is %(name)r, now give me %(obj)r"

P_NAME: Final = "Klaatu"
P_RS: Final = ReprStr()

RES_DEBUG_NAME: Final = "Hello, Klaatu!"
RES_ERR_RS: Final = "Do we have <a ReprStr object>?"
RES_INFO_NAME_RS_R: Final = "The name is 'Klaatu', now give me ReprStr()"


@pytest.mark.parametrize(
    ("verbose", "use_mock"),
    [
        (False, False),
        (False, True),
        (True, False),
        (True, True),
    ],
)
def test_trivial(*, verbose: bool, use_mock: bool) -> None:
    """Test logging a couple of messages."""
    io_out, io_err, log = util.build_test_logger(verbose=verbose, use_mock=use_mock)

    log.error(MSG_ERR)
    log.debug(MSG_DEBUG)
    log.warning(MSG_WARNING)
    log.info(MSG_INFO)
    log.debug(MSG_DEBUG_2)

    res_out: Final = io_out.getvalue()
    res_err: Final = io_err.getvalue()
    assert (res_out, res_err) == (
        f"{MSG_INFO}\n",
        f"{MSG_ERR}\n{MSG_DEBUG}\n{MSG_WARNING}\n{MSG_DEBUG_2}\n"
        if verbose
        else f"{MSG_ERR}\n{MSG_WARNING}\n",
    )


@pytest.mark.parametrize(
    ("verbose", "use_mock"),
    [
        (False, False),
        (False, True),
        (True, False),
        (True, True),
    ],
)
def test_trivial_single(*, verbose: bool, use_mock: bool) -> None:
    """Test logging a couple of messages."""
    io_err, log = util.build_test_single_logger(verbose=verbose, use_mock=use_mock)

    log.error(MSG_ERR)
    log.debug(MSG_DEBUG)
    log.warning(MSG_WARNING)
    log.info(MSG_INFO)
    log.debug(MSG_DEBUG_2)

    res_err: Final = io_err.getvalue()
    assert (
        res_err == f"{MSG_ERR}\n{MSG_DEBUG}\n{MSG_WARNING}\n{MSG_INFO}\n{MSG_DEBUG_2}\n"
        if verbose
        else f"{MSG_ERR}\n{MSG_WARNING}\n{MSG_INFO}\n"
    )


@pytest.mark.parametrize(
    ("verbose", "use_mock"),
    [
        (False, False),
        (False, True),
        (True, False),
        (True, True),
    ],
)
def test_args(*, verbose: bool, use_mock: bool) -> None:
    """Test logging a couple of messages with arguments."""
    io_out, io_err, log = util.build_test_logger(verbose=verbose, use_mock=use_mock)

    log.debug(MSG_DEBUG_NAME, {"name": P_NAME})
    log.error(MSG_ERR_RS, {"obj": P_RS})
    log.info(MSG_INFO_NAME_RS_R, {"name": P_NAME, "obj": P_RS})

    res_out: Final = io_out.getvalue()
    res_err: Final = io_err.getvalue()
    assert (res_out, res_err) == (
        f"{RES_INFO_NAME_RS_R}\n",
        f"{RES_DEBUG_NAME}\n{RES_ERR_RS}\n" if verbose else f"{RES_ERR_RS}\n",
    )


@pytest.mark.parametrize(
    ("verbose", "use_mock"),
    [
        (False, False),
        (False, True),
        (True, False),
        (True, True),
    ],
)
def test_args_single(*, verbose: bool, use_mock: bool) -> None:
    """Test logging a couple of messages with arguments."""
    io_err, log = util.build_test_single_logger(verbose=verbose, use_mock=use_mock)

    log.debug(MSG_DEBUG_NAME, {"name": P_NAME})
    log.error(MSG_ERR_RS, {"obj": P_RS})
    log.info(MSG_INFO_NAME_RS_R, {"name": P_NAME, "obj": P_RS})

    res_err: Final = io_err.getvalue()
    assert (
        res_err == f"{RES_DEBUG_NAME}\n{RES_ERR_RS}\n{RES_INFO_NAME_RS_R}\n"
        if verbose
        else f"{RES_ERR_RS}\n{RES_INFO_NAME_RS_R}\n"
    )
