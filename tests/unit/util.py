# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Helper functions for the `logging_std` library's unit tests."""

from __future__ import annotations

import io
import sys
import typing
from unittest import mock

import logging_std


if typing.TYPE_CHECKING:
    import logging
    from typing import Final


def build_test_logger(
    *,
    verbose: bool,
    use_mock: bool = False,
) -> tuple[io.StringIO, io.StringIO, logging.Logger]:
    """Build the test logger and the two output streams."""
    io_out: Final = io.StringIO()
    io_err: Final = io.StringIO()

    if use_mock:
        with mock.patch.object(sys, "stdout", io_out), mock.patch.object(sys, "stderr", io_err):
            log = logging_std.build_logger.__wrapped__(verbose=verbose)
    else:
        log = logging_std.build_logger.__wrapped__(
            verbose=verbose,
            error_stream=io_err,
            info_stream=io_out,
        )

    return io_out, io_err, log


def build_test_single_logger(
    *,
    verbose: bool,
    use_mock: bool = False,
) -> tuple[io.StringIO, logging.Logger]:
    """Build the test logger and the two output streams."""
    io_err: Final = io.StringIO()

    if use_mock:
        with mock.patch.object(sys, "stderr", io_err):
            log = logging_std.build_single_logger.__wrapped__(verbose=verbose)
    else:
        log = logging_std.build_single_logger.__wrapped__(verbose=verbose, stream=io_err)

    return io_err, log
